package br.com.wagner.client.impl.builder;

import br.com.wagner.client.impl.model.NumeroPedidoModel;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NumeroPedidoBuilder {

    public static NumeroPedidoModel buildNumeroPedidoModel(Long numeroPedido){
        return NumeroPedidoModel.builder()
                .id(numeroPedido)
                .build();
    }
}
