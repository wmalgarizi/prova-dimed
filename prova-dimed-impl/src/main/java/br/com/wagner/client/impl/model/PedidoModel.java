package br.com.wagner.client.impl.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by wmalgarizi on 19/04/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PedidoModel {

    private Long id;
    private List<ItemModel> itens;
    private Long idCliente;
    private Long idEndereco;
    private Double valorPedido;
    private EntregaModel entrega;
    private FormaPagamentoModel formaPagamento;
}
