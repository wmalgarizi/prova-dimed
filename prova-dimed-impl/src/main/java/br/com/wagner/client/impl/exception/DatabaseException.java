package br.com.wagner.client.impl.exception;

import javax.ws.rs.core.Response;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class DatabaseException extends ApiException {
    @Override
    public Integer code() {
        return null;
    }

    @Override
    public String message() {
        return "Erro na conexão com o banco. Caso o problema persista, entre em contato com o suporte";
    }

    @Override
    public Response.Status httpCode() {
        return Response.Status.SERVICE_UNAVAILABLE;
    }
}
