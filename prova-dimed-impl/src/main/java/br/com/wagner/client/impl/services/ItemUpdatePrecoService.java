package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.ItemData;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;
import org.mybatis.spring.MyBatisSystemException;

import java.util.List;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemUpdatePrecoService {

    private ItemData itemData;

    public ItemUpdatePrecoService(ItemData itemData) {
        this.itemData = itemData;
    }

    public void alterarPreco(List<ItemUpdatePrecoModel> itemUpdatePrecoModels) {
        try {
            itemUpdatePrecoModels.forEach(itemData::alterarPreco);
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
