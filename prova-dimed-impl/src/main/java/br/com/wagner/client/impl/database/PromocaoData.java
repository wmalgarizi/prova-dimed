package br.com.wagner.client.impl.database;

import br.com.wagner.client.impl.model.NovaPromocaoModel;
import br.com.wagner.client.impl.model.PromocaoModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by wmalgarizi on 17/04/17.
 */

public interface PromocaoData {

    @Insert("INSERT INTO promocao " +
            "(id_item, percentual_desconto) " +
            "VALUES " +
            "(#{idItem}, #{novaPromocao.percentualDesconto})")
    public Boolean cadastrarPromocao(@Param("idItem") Long idItem,
                                     @Param("novaPromocao") PromocaoModel promocaoModel);

    @Select("SELECT codigo FROM promocao JOIN item on item.id_item = promocao.id_item WHERE codigo = #{codigoPromocao} AND item.id_item = #{idItem} ")
    public Boolean verificaCodigoPromocao(@Param("codigoPromocao") Long codigoPromocao,
                                          @Param("idItem") Long idItem);
}
