package br.com.wagner.client.impl.model;

import lombok.Getter;

/**
 * Created by wmalgarizi on 19/04/17.
 */

@Getter
public enum EntregaModel {

    MOTOBOY("motoboy", 1L),
    SEDEX("sedex", 2L),
    UNKNOW("", -1L);

    private String value;
    private Long codigoEntrega;

    EntregaModel(String value, Long codigoEntrega) {
        this.value = value;
        this.codigoEntrega = codigoEntrega;
    }

    public static EntregaModel fromValue(String value) {
        for (EntregaModel entrega : EntregaModel.values()) {
            if (entrega.getValue().equals(value.toLowerCase())) return entrega;
        }
        return UNKNOW;
    }
}
