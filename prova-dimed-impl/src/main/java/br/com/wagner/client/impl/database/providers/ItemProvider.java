package br.com.wagner.client.impl.database.providers;

import java.util.Map;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemProvider {

    public static String selecionaItensPorNome(Map params) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT item.id_item, item.nome, item.descricao, item.preco, " +
                "promocao.codigo, promocao.percentual_desconto " +
                "FROM item " +
                "LEFT OUTER JOIN promocao on promocao.id_item = item.id_item " +
                "WHERE item.id_item IS NOT NULL " +
                "AND item.nome like " + "'%" + params.get("nomeItem") + "%'");
        return sql.toString();
    }

    public static String selecionaItemPorId(Map params) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT item.id_item, item.nome, item.descricao, item.preco, " +
                "promocao.codigo, promocao.percentual_desconto " +
                "FROM item " +
                "LEFT OUTER JOIN promocao on promocao.id_item = item.id_item " +
                "WHERE item.id_item = "+ params.get("idItem") + "");
        return sql.toString();
    }
}
