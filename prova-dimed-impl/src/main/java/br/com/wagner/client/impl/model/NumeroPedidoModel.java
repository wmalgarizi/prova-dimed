package br.com.wagner.client.impl.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by wmalgarizi on 19/04/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NumeroPedidoModel {

    private Long id;
}
