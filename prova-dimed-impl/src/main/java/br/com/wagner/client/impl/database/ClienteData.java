package br.com.wagner.client.impl.database;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public interface ClienteData {

    @Select("SELECT id_cliente FROM cliente WHERE id_cliente = #{idCliente}")
    public Boolean verificaIdCliente(@Param("idCliente") Long idCliente);

    @Select("SELECT endereco.id_endereco FROM endereco " +
            "JOIN cliente on endereco.id_endereco = cliente.id_endereco " +
            "WHERE endereco.id_endereco = #{idEndereco}")
    public Boolean verificaIdEndereco(@Param("idEndereco") Long idEndereco);
}
