package br.com.wagner.client.impl.database;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public interface FormaPagamentoData {

    @Select("SELECT id_forma_pagamento FROM forma_pagamento WHERE id_forma_pagamento = #{idFormaPagamento}")
    public Boolean verificaIdFormaPagamento(@Param("idFormaPagamento") Long idFormaPagamento);
}
