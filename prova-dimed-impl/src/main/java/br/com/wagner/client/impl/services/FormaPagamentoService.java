package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.FormaPagamentoData;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.model.FormaPagamentoModel;
import org.mybatis.spring.MyBatisSystemException;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public class FormaPagamentoService {

    private FormaPagamentoData formaPagamentoData;

    public FormaPagamentoService(FormaPagamentoData formaPagamentoData) {
        this.formaPagamentoData = formaPagamentoData;
    }

    public Boolean verificaIdFormaPagamento(FormaPagamentoModel formaPagamentoModel){
        try {
            if (FormaPagamentoModel.UNKNOW.equals(formaPagamentoModel)) return false;
            Boolean result = formaPagamentoData.verificaIdFormaPagamento(formaPagamentoModel.getCodigoFormaPagamento());
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
