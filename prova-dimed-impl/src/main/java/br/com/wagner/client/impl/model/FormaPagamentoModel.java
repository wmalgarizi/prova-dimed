package br.com.wagner.client.impl.model;

import lombok.Getter;

/**
 * Created by wmalgarizi on 19/04/17.
 */

@Getter
public enum FormaPagamentoModel {

    CREDITO("credito", 1L),
    DEBITO("debito", 2L),
    DINHEIRO("dinheiro", 3L),
    UNKNOW("", -1L);

    private String value;
    private Long codigoFormaPagamento;

    FormaPagamentoModel(String value, Long codigoFormaPagamento) {
        this.value = value;
        this.codigoFormaPagamento = codigoFormaPagamento;
    }

    public static FormaPagamentoModel fromValue(String value){
        for (FormaPagamentoModel formaPagamento: FormaPagamentoModel.values()) {
            if (formaPagamento.getValue().equals(value.toLowerCase()))
                return formaPagamento;
        }
        return UNKNOW;
    }
}
