package br.com.wagner.client.impl.database.providers;

import br.com.wagner.client.impl.model.PedidoModel;
import br.com.wagner.client.impl.model.PromocaoModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Optional;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public class ItensDoPedidoProvider {

    public static String inserirItensDoPedido(Map params){

        PedidoModel pedidoModel = (PedidoModel) params.get("pedidoModel");
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO itens_do_pedido " +
                "(id_pedido, codigo_promocao, id_item)" +
                "VALUES ");

        pedidoModel.getItens().forEach(item -> {
            sql.append("(" + pedidoModel.getId() + "," + Optional.ofNullable(item.getPromocao()).map(PromocaoModel::getCodigo).orElse(null) + "," + item.getId() + "),");
        });

        return sql.toString().substring(0, sql.toString().length() - 1);
    }
}
