package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.ItemData;
import br.com.wagner.client.impl.exception.AlreadyExistException;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.exception.NothingFoundException;
import br.com.wagner.client.impl.model.ItemModel;
import br.com.wagner.client.impl.model.NovoItemModel;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemService {

    private ItemData itemData;
    private PromocaoService promocaoService;

    public ItemService(ItemData itemData, PromocaoService promocaoService) {
        this.itemData = itemData;
        this.promocaoService = promocaoService;
    }

    public void  cadastrarItem(ItemModel ItemModel) {
        try {
            itemData.cadastrarItem(ItemModel);
            promocaoService.cadastrarPromocao(ItemModel.getId(), ItemModel.getPromocao());
        } catch (DuplicateKeyException error) {
            throw new AlreadyExistException("Este item já existe");
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    public ItemModel procurarItemPorID(Long idItem){
        try {
            return itemData.procurarItemPorId(idItem);
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    public List<ItemModel> procurarItensPorNome(String nomeItem) {
        try {
            List<ItemModel> itens = itemData.procurarItensPorNome(nomeItem);
            if (itens.isEmpty())
                throw new NothingFoundException("Nenhum item encontrado");
            return itens;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    public Boolean verificaIdItem(Long idItem){
        try {
            Boolean result = itemData.verificaIdItem(idItem);
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

}
