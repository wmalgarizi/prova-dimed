package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.ClienteData;
import br.com.wagner.client.impl.exception.DatabaseException;
import org.mybatis.spring.MyBatisSystemException;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public class ClienteService {

    private ClienteData clienteData;

    public ClienteService(ClienteData clienteData) {
        this.clienteData = clienteData;
    }

    public Boolean verificaIdCliente(Long idCliente){
        try {
            Boolean result = clienteData.verificaIdCliente(idCliente);
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    public Boolean verificaIdEndereco(Long idEndereco){
        try {
            Boolean result = clienteData.verificaIdEndereco(idEndereco);
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
