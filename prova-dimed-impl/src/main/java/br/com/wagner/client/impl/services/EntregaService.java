package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.EntregaData;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.model.EntregaModel;
import org.mybatis.spring.MyBatisSystemException;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public class EntregaService {

    private EntregaData entregaData;

    public EntregaService(EntregaData entregaData) {
        this.entregaData = entregaData;
    }

    public Boolean verificaIdEntrega(EntregaModel entregaModel){
        try {
            if (EntregaModel.UNKNOW.equals(entregaModel)) return false;
            Boolean result = entregaData.verificaIdEntrega(entregaModel.getCodigoEntrega());
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
