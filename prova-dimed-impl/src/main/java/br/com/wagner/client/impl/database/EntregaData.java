package br.com.wagner.client.impl.database;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public interface EntregaData {

    @Select("SELECT id_entrega FROM entrega WHERE id_entrega = #{idEntrega}")
    public Boolean verificaIdEntrega(@Param("idEntrega") Long idEntrega);
}
