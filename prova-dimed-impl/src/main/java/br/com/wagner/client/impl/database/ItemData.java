package br.com.wagner.client.impl.database;

import br.com.wagner.client.impl.database.providers.ItemProvider;
import br.com.wagner.client.impl.model.ItemModel;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;
import br.com.wagner.client.impl.model.NovoItemModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public interface ItemData {

    @Insert("INSERT INTO item " +
            "(nome, descricao, preco) " +
            "VALUES " +
            "(#{novoItem.nome}, #{novoItem.descricao}, #{novoItem.preco})")
    @Options(useGeneratedKeys = true, keyProperty = "novoItem.id", keyColumn = "id_item")
    public Boolean cadastrarItem(@Param("novoItem") ItemModel ItemModel);

    @SelectProvider(type = ItemProvider.class, method = "selecionaItemPorId")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "nome", property = "nome"),
            @Result(column = "descricao", property = "descricao"),
            @Result(column = "preco", property = "preco"),
            @Result(column = "codigo", property = "promocao.codigo"),
            @Result(column = "percentual_desconto", property = "promocao.percentualDesconto")
    })
    public ItemModel procurarItemPorId(@Param("idItem") Long idItem);

    @SelectProvider(type = ItemProvider.class, method = "selecionaItensPorNome")
    @Results(value = {
            @Result(column = "id_item", property = "id"),
            @Result(column = "nome", property = "nome"),
            @Result(column = "descricao", property = "descricao"),
            @Result(column = "preco", property = "preco"),
            @Result(column = "codigo", property = "promocao.codigo"),
            @Result(column = "percentual_desconto", property = "promocao.percentualDesconto")
    })
    public List<ItemModel> procurarItensPorNome(@Param("nomeItem") String nomeItem);


    @Update("UPDATE item " +
            "SET preco = #{itemUpdate.preco} " +
            "WHERE id_item = #{itemUpdate.id}")
    public Boolean alterarPreco(@Param("itemUpdate") ItemUpdatePrecoModel itemUpdatePrecoModel);

    @Select("SELECT id_item FROM item where id_item = #{idItem}")
    public Boolean verificaIdItem(@Param("idItem") Long idItem);
}
