package br.com.wagner.client.impl.database;

import br.com.wagner.client.impl.database.providers.ItensDoPedidoProvider;
import br.com.wagner.client.impl.model.PedidoModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public interface PedidoData {

    @Insert("INSERT INTO pedido " +
            "(id_cliente, id_endereco, id_entrega, id_forma_pagamento, valor_pedido) " +
            "VALUES" +
            "(#{novoPedido.idCliente}, #{novoPedido.idEndereco}, #{novoPedido.entrega.codigoEntrega}, #{novoPedido.formaPagamento.codigoFormaPagamento}, #{novoPedido.valorPedido}) ")
    @Options(useGeneratedKeys = true, keyColumn = "id_pedido", keyProperty = "novoPedido.id")
    public Boolean inserirPedido(@Param("novoPedido")PedidoModel pedidoModel);

    @InsertProvider(type = ItensDoPedidoProvider.class, method = "inserirItensDoPedido")
    public Boolean inserirItensDoPedido(@Param("pedidoModel") PedidoModel pedidoModel);
}
