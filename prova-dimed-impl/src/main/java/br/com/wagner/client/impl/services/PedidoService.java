package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.builder.NumeroPedidoBuilder;
import br.com.wagner.client.impl.database.PedidoData;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.model.NumeroPedidoModel;
import br.com.wagner.client.impl.model.PedidoModel;
import org.mybatis.spring.MyBatisSystemException;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class PedidoService {

    private PedidoData pedidoData;

    public PedidoService(PedidoData pedidoData) {
        this.pedidoData = pedidoData;
    }

    public NumeroPedidoModel finalizarPedido(PedidoModel pedidoModel){
        try {
            pedidoData.inserirPedido(pedidoModel);
            inserirItensDoPedido(pedidoModel);
            return NumeroPedidoBuilder.buildNumeroPedidoModel(pedidoModel.getId());
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    private void inserirItensDoPedido(PedidoModel pedidoModel){
        try {
            pedidoData.inserirItensDoPedido(pedidoModel);
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
