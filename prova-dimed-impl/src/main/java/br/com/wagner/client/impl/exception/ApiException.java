package br.com.wagner.client.impl.exception;

import javax.ws.rs.core.Response;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public abstract class ApiException extends RuntimeException {

    public abstract Integer code();

    public abstract String message();

    public abstract Response.Status httpCode();
}
