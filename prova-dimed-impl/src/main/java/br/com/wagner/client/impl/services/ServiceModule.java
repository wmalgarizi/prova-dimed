package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wmalgarizi on 17/04/17.
 */

@Configuration
public class ServiceModule {

    @Bean
    public ItemService itemService(ItemData itemData, PromocaoService promocaoService) {
        return new ItemService(itemData, promocaoService);
    }

    @Bean
    public ItemUpdatePrecoService itemUpdatePrecoService(ItemData itemData) {
        return new ItemUpdatePrecoService(itemData);
    }

    @Bean
    public PromocaoService promocaoService(PromocaoData promocaoData) {
        return new PromocaoService(promocaoData);
    }

    @Bean
    public PedidoService pedidoService(PedidoData pedidoData){
        return new PedidoService(pedidoData);
    }

    @Bean
    public ClienteService clienteService(ClienteData clienteData){
        return new ClienteService(clienteData);
    }

    @Bean
    public EntregaService entregaService(EntregaData entregaData){
        return new EntregaService(entregaData);
    }

    @Bean
    public FormaPagamentoService formaPagamentoService(FormaPagamentoData formaPagamentoData){
        return new FormaPagamentoService(formaPagamentoData);
    }
}
