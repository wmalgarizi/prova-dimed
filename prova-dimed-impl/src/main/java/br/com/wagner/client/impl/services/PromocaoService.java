package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.PromocaoData;
import br.com.wagner.client.impl.exception.AlreadyExistException;
import br.com.wagner.client.impl.exception.DatabaseException;
import br.com.wagner.client.impl.model.NovaPromocaoModel;
import br.com.wagner.client.impl.model.PromocaoModel;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.dao.DuplicateKeyException;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class PromocaoService {

    private PromocaoData promocaoData;

    public PromocaoService(PromocaoData promocaoData) {
        this.promocaoData = promocaoData;
    }

    public void cadastrarPromocao(Long idItem, PromocaoModel promocaoModel) {
        try {
            promocaoData.cadastrarPromocao(idItem, promocaoModel);
        }catch (DuplicateKeyException error){
            throw new AlreadyExistException("A promoção já existe.");
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }

    public Boolean verificaCodigoPromocao(Long codigoPromocao, Long idItem){
        try {
            Boolean result = promocaoData.verificaCodigoPromocao(codigoPromocao, idItem);
            if (result == null) return false;
            return result;
        }catch (MyBatisSystemException error){
            throw new DatabaseException();
        }
    }
}
