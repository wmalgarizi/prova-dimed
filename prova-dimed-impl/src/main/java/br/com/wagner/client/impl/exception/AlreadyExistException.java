package br.com.wagner.client.impl.exception;

import javax.ws.rs.core.Response;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class AlreadyExistException extends ApiException {

    private String message;

    public AlreadyExistException(String message) {
        this.message = message;
    }

    @Override
    public Integer code() {
        return null;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public Response.Status httpCode() {
        return Response.Status.CONFLICT;
    }
}
