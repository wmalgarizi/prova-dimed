package br.com.wagner.client.impl.services;

import br.com.wagner.client.impl.database.ItemData;
import br.com.wagner.client.impl.exception.NothingFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class ItemServiceTest {

    private ItemService itemService;
    private ItemData itemData;
    private PromocaoService promocaoService;

    @Before
    public void setUp(){
        this.itemData = mock(ItemData.class);
        when(itemData.procurarItensPorNome("creme")).thenReturn(new ArrayList<>());
        this.promocaoService = mock(PromocaoService.class);
        this.itemService = new ItemService(itemData, promocaoService);
    }

    @Test(expected = NothingFoundException.class)
    public void verificaNothingFoundExceptionProcurarItens(){
        itemService.procurarItensPorNome("creme");
    }




}