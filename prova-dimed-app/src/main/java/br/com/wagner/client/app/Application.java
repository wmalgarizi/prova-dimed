package br.com.wagner.client.app;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by wmalgarizi on 17/04/17.
 */

@EnableAutoConfiguration
@ComponentScan(basePackages = "br.com.wagner")
public class Application {

    public static void main(String[] args) {
            new SpringApplicationBuilder(Application.class)
                    .listeners(new ApplicationPidFileWriter())
                    .run(args);
    }
}
