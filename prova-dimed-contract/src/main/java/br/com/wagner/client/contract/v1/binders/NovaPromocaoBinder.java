package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.NovaPromocao;
import br.com.wagner.client.impl.model.NovaPromocaoModel;
import br.com.wagner.client.impl.model.PromocaoModel;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NovaPromocaoBinder {

    public static PromocaoModel bindParaPromocaoModel(NovaPromocao novaPromocao){
        return PromocaoModel.builder()
                .percentualDesconto(novaPromocao.getPercentualDesconto())
                .build();
    }
}
