package br.com.wagner.client.contract.v1;

import br.com.wagner.client.contract.v1.exception.ExceptionModel;
import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.facade.ItemFacade;
import br.com.wagner.client.contract.v1.facade.ItemUpdatePrecoFacade;
import br.com.wagner.client.contract.v1.facade.PedidoFacade;
import br.com.wagner.client.contract.v1.model.*;
import br.com.wagner.client.contract.v1.validator.ItemUpdatePrecoValidator;
import br.com.wagner.client.contract.v1.validator.NovoItemValidator;
import br.com.wagner.client.contract.v1.validator.PedidoValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by wmalgarizi on 17/04/17.
 */

@Path("/v1")
@Api(value = "Loja teste", description = "Prova Dimed")
public class ItemEndPoint {

    @Autowired
    private ItemFacade itemFacade;

    @Autowired
    private ItemUpdatePrecoFacade itemUpdatePrecoFacade;

    @Autowired
    private PedidoFacade pedidoFacade;

    @POST
    @Path("/cadastrar")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Recebe um modelo de NovoItem para cadastro.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, response = Void.class, message = "Success!!"),
            @ApiResponse(code = 400, response = ExceptionModel.class, message = "Modelo de NovoItem inválido"),
            @ApiResponse(code = 409, response = ExceptionModel.class, message = "Item já existente"),
            @ApiResponse(code = 503, response = ExceptionModel.class, message = "Serviço indisponível")
    })
    public void cadastrarNovoItem(NovoItem novoItem) {
        NovoItemValidator.validaNovoItem(novoItem);
        itemFacade.cadastrarNovoItem(novoItem);
    }

    @PUT
    @Path("/alterarpreco")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Recebe um modelo de ItemUpdatePreco para alteracao de valor do item.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, response = Void.class, message = "Success!!"),
            @ApiResponse(code = 400, response = ExceptionModel.class, message = "Modelo de ItemUpdatePreco inválido"),
            @ApiResponse(code = 503, response = ExceptionModel.class, message = "Serviço indisponível")
    })
    public void alterarPrecoItem(List<ItemUpdatePreco> itensUpdatePreco) {
        ItemUpdatePrecoValidator.validaListaItemUpdatePreco(itensUpdatePreco);
        itemUpdatePrecoFacade.alterarPrecoItem(itensUpdatePreco);
    }

    @GET
    @Path("/procuraporid")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Busca o item por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Item.class, message = "Success"),
            @ApiResponse(code = 404, response = ExceptionModel.class, message = "Item não encontrado"),
            @ApiResponse(code = 503, response = ExceptionModel.class, message = "Serviço indisponível")
    })
    public Item pegarItemPorId(@QueryParam("idItem") Long idItem) {
        if (idItem == null || idItem == 0)
            throw new InputException("Informe um Id para a pesquisa");
        return itemFacade.procurarItemPorId(idItem);
    }

    @GET
    @Path("/procurapornome")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Busca itens por nome")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Item[].class, message = "Success"),
            @ApiResponse(code = 404, response = ExceptionModel.class, message = "Item não encontrado"),
            @ApiResponse(code = 503, response = ExceptionModel.class, message = "Serviço indisponível")
    })
    public List<Item> pegarItens(@QueryParam("nomeItem") String nomeItem) {
        if (nomeItem == null || nomeItem.equals(""))
            throw new InputException("Informe um nome para a pesquisa");
        return itemFacade.procurarItensPorNome(nomeItem);
    }

    @POST
    @Path("/finalizapedido")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Finaliza o pedido")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = NumeroPedido.class, message = "Success"),
            @ApiResponse(code = 404, response = ExceptionModel.class, message = "Item não encontrado"),
            @ApiResponse(code = 503, response = ExceptionModel.class, message = "Serviço indisponível")
    })
    public NumeroPedido finalizarPedido(Pedido pedido){
        PedidoValidator.validaPedido(pedido);
        return pedidoFacade.finalizaPedido(pedido);
    }

}
