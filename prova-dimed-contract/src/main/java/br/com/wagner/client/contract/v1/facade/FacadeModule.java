package br.com.wagner.client.contract.v1.facade;

import br.com.wagner.client.impl.services.*;
import lombok.Builder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wmalgarizi on 17/04/17.
 */

@Configuration
public class FacadeModule {

    @Bean
    public ItemFacade itemFacade(ItemService itemService) {
        return new ItemFacade(itemService);
    }

    @Bean
    public ItemUpdatePrecoFacade itemUpdatePrecoFacade(ItemUpdatePrecoService itemUpdatePrecoService){
        return new ItemUpdatePrecoFacade(itemUpdatePrecoService);
    }

    @Bean
    public PedidoFacade pedidoFacade(PedidoService pedidoService, ClienteService clienteService, EntregaService entregaService, FormaPagamentoService formaPagamentoService, ItemService itemService, PromocaoService promocaoService){
        return new PedidoFacade(pedidoService, clienteService, entregaService, formaPagamentoService, itemService, promocaoService);
    }
}
