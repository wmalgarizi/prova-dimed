package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.calculator.ValorPedidoCalculator;
import br.com.wagner.client.contract.v1.mapper.ItemMapper;
import br.com.wagner.client.contract.v1.model.Pedido;
import br.com.wagner.client.impl.model.EntregaModel;
import br.com.wagner.client.impl.model.FormaPagamentoModel;
import br.com.wagner.client.impl.model.PedidoModel;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class PedidoBinder {

    public static PedidoModel bindParaImpl(Pedido pedido){
        return PedidoModel.builder()
                .itens(ItemMapper.mapListParaImpl(pedido.getItens()))
                .idCliente(pedido.getIdCliente())
                .idEndereco(pedido.getIdEndereco())
                .valorPedido(ValorPedidoCalculator.calculaValorPedido(pedido.getItens()))
                .entrega(EntregaModel.fromValue(pedido.getEntrega()))
                .formaPagamento(FormaPagamentoModel.fromValue(pedido.getFormaPagamento()))
                .build();
    }
}
