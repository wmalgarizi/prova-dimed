package br.com.wagner.client.contract.v1.exception;

import lombok.Builder;
import lombok.Data;

/**
 * Created by wmalgarizi on 18/04/17.
 */

@Data
@Builder
public class ExceptionModel {

    private Integer code;
    private String message;
}
