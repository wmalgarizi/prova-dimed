package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.NovoItem;
import br.com.wagner.client.impl.model.ItemModel;
import br.com.wagner.client.impl.model.NovoItemModel;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NovoItemBinder {

    public static ItemModel bindParaItemModel(NovoItem novoItem){
        return ItemModel.builder()
                .nome(novoItem.getNome())
                .descricao(novoItem.getDescricao())
                .preco(novoItem.getPreco())
                .promocao(NovaPromocaoBinder.bindParaPromocaoModel(novoItem.getPromocao()))
                .build();
    }
}
