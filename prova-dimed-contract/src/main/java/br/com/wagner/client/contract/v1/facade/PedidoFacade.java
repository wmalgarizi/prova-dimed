package br.com.wagner.client.contract.v1.facade;

import br.com.wagner.client.contract.v1.binders.NumeroPedidoBinder;
import br.com.wagner.client.contract.v1.binders.PedidoBinder;
import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.NumeroPedido;
import br.com.wagner.client.contract.v1.model.Pedido;
import br.com.wagner.client.impl.model.EntregaModel;
import br.com.wagner.client.impl.model.FormaPagamentoModel;
import br.com.wagner.client.impl.model.PedidoModel;
import br.com.wagner.client.impl.services.*;

import java.util.Optional;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class PedidoFacade {

    private PedidoService pedidoService;
    private ClienteService clienteService;
    private EntregaService entregaService;
    private FormaPagamentoService formaPagamentoService;
    private ItemService itemService;
    private PromocaoService promocaoService;

    public PedidoFacade(PedidoService pedidoService, ClienteService clienteService, EntregaService entregaService, FormaPagamentoService formaPagamentoService, ItemService itemService, PromocaoService promocaoService) {
        this.pedidoService = pedidoService;
        this.clienteService = clienteService;
        this.entregaService = entregaService;
        this.formaPagamentoService = formaPagamentoService;
        this.itemService = itemService;
        this.promocaoService = promocaoService;
    }

    public NumeroPedido finalizaPedido(Pedido pedido){
        validarDadosPedido(pedido);
        PedidoModel pedidoModel = PedidoBinder.bindParaImpl(pedido);
        return NumeroPedidoBinder.bindParaContract(pedidoService.finalizarPedido(pedidoModel));
    }

    private void validarDadosPedido(Pedido pedido){
        pedido.getItens().forEach(item -> {
            if (!itemService.verificaIdItem(item.getId()))
                throw new InputException("Item informado não existe.");
            else if (item.getPromocao() != null && item.getPromocao().getCodigo() != null && !promocaoService.verificaCodigoPromocao(item.getPromocao().getCodigo(), item.getId()))
                throw new InputException("A promoção informada é inválida para o item " + item.getId());
        });
        if (!clienteService.verificaIdCliente(pedido.getIdCliente()))
            throw new InputException("O cliente informado não existe.");
        if (!clienteService.verificaIdEndereco(pedido.getIdEndereco()))
            throw new InputException("O endereço informado não existe ou não pertence a este cliente.");
        if (!entregaService.verificaIdEntrega(EntregaModel.fromValue(pedido.getEntrega())))
            throw new InputException("O tipo de entrega selecionado não existe.");
        if (!formaPagamentoService.verificaIdFormaPagamento(FormaPagamentoModel.fromValue(pedido.getFormaPagamento())))
            throw new InputException("A forma de pagamento selecionada não existe.");
    }
}
