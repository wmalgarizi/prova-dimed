package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.ItemUpdatePreco;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemUpdatePrecoBinder {

    public static ItemUpdatePreco bindParaContract(ItemUpdatePrecoModel itemUpdatePrecoModel) {
        return ItemUpdatePreco.builder()
                .id(itemUpdatePrecoModel.getId())
                .preco(itemUpdatePrecoModel.getPreco())
                .build();
    }

    public static ItemUpdatePrecoModel bindParaImpl(ItemUpdatePreco itemUpdatePreco) {
        return ItemUpdatePrecoModel.builder()
                .id(itemUpdatePreco.getId())
                .preco(itemUpdatePreco.getPreco())
                .build();
    }
}
