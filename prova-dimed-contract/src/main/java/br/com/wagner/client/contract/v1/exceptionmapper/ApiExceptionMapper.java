package br.com.wagner.client.contract.v1.exceptionmapper;

import br.com.wagner.client.contract.v1.exception.ExceptionModel;
import br.com.wagner.client.impl.exception.ApiException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class ApiExceptionMapper implements ExceptionMapper<ApiException> {
    @Override
    public Response toResponse(ApiException exception) {
        return Response.status(exception.httpCode()).
                entity(buildExceptionModel(exception)).
                type(MediaType.APPLICATION_JSON).
                build();
    }

    private ExceptionModel buildExceptionModel(ApiException exception) {
        return ExceptionModel.builder()
                .code(exception.code())
                .message(exception.message())
                .build();
    }
}
