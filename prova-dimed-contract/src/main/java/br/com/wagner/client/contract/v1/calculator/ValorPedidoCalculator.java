package br.com.wagner.client.contract.v1.calculator;

import br.com.wagner.client.contract.v1.model.Item;
import br.com.wagner.client.contract.v1.model.Promocao;
import br.com.wagner.client.impl.model.PromocaoModel;

import java.util.List;
import java.util.Optional;

/**
 * Created by wmalgarizi on 20/04/17.
 */
public class ValorPedidoCalculator {

    public static Double calculaValorPedido(List<Item> itens){
        Double valorTotal = 0.0;
        for (Item item : itens) {
            Double precoItem = item.getPreco();
            Double percentualDesconto = Optional.ofNullable(item.getPromocao()).map(Promocao::getPercentualDesconto).orElse(0D);
            if (percentualDesconto != 0)
                valorTotal += (precoItem - (precoItem *(percentualDesconto / 100)));
            else
                valorTotal += precoItem;
        }
        return valorTotal;
    }
}
