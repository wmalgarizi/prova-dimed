package br.com.wagner.client.config;

import br.com.wagner.client.contract.v1.ItemEndPoint;
import br.com.wagner.client.contract.v1.exceptionmapper.ApiExceptionMapper;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * Created by wmalgarizi on 17/04/17.
 */

@Component
@ApplicationPath("/lojavirtual")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        super();
        register(ItemEndPoint.class);
        register(ApiExceptionMapper.class);
        EncodingFilter.enableFor(this, GZipEncoder.class);
    }
}
