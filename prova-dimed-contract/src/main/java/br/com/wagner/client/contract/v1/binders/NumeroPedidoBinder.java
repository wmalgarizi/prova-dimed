package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.NumeroPedido;
import br.com.wagner.client.impl.model.NumeroPedidoModel;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NumeroPedidoBinder {

    public static NumeroPedido bindParaContract(NumeroPedidoModel numeroPedidoModel){
        return NumeroPedido.builder()
                .id(numeroPedidoModel.getId())
                .build();
    }
}
