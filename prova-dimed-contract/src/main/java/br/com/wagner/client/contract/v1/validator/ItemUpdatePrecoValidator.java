package br.com.wagner.client.contract.v1.validator;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.ItemUpdatePreco;

import java.util.List;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class ItemUpdatePrecoValidator {

    public static void validaListaItemUpdatePreco(List<ItemUpdatePreco> itensUpdatePreco){
        if (itensUpdatePreco == null || itensUpdatePreco.isEmpty())
            throw new InputException("Nenhuma lista de itens recebida");
        itensUpdatePreco.forEach(ItemUpdatePrecoValidator::validaItemUpdatePreco);
    }

    public static void validaItemUpdatePreco(ItemUpdatePreco itemUpdatePreco){
        if (itemUpdatePreco == null)
            throw new InputException("Nenhum item recebido");
        if (itemUpdatePreco.getId() == null || itemUpdatePreco.getId() == 0)
            throw new InputException("O id do item deve ser informado.");
        if (itemUpdatePreco.getPreco() == null || itemUpdatePreco.getPreco() == 0.0)
            throw new InputException("O preço do item deve ser informado");
    }
}
