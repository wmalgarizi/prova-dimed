package br.com.wagner.client.contract.v1.validator;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.NovaPromocao;
import io.swagger.models.auth.In;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NovaPromocaoValidator {

    public static void validaNovaPromocao(NovaPromocao novaPromocao){
        if (novaPromocao != null) {
            if (novaPromocao.getPercentualDesconto() == null)
                throw new InputException("O percentual de desconto da promoção deve ser informado");
        }
    }
}
