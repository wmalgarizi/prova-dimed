package br.com.wagner.client.contract.v1.mapper;

import br.com.wagner.client.contract.v1.binders.PromocaoBinder;
import br.com.wagner.client.contract.v1.model.Item;
import br.com.wagner.client.impl.model.ItemModel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemMapper {

    public static List<Item> mapListParaContract(List<ItemModel> itemModels) {
        return itemModels.stream().map(ItemMapper::mapParaContract).collect(Collectors.toList());
    }

    public static Item mapParaContract(ItemModel itemModel) {
        return Item.builder()
                .id(itemModel.getId())
                .nome(itemModel.getNome())
                .descricao(itemModel.getDescricao())
                .preco(itemModel.getPreco())
                .promocao(PromocaoBinder.bindParaContract(itemModel.getPromocao()))
                .build();
    }

    public static List<ItemModel> mapListParaImpl(List<Item> itens) {
        return itens.stream().map(ItemMapper::mapParaImpl).collect(Collectors.toList());
    }

    public static ItemModel mapParaImpl(Item item) {
        return ItemModel.builder()
                .id(item.getId())
                .nome(item.getNome())
                .descricao(item.getDescricao())
                .preco(item.getPreco())
                .promocao(PromocaoBinder.bindParaImpl(item.getPromocao()))
                .build();
    }
}
