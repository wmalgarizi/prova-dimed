package br.com.wagner.client.contract.v1.exception;

import br.com.wagner.client.impl.exception.ApiException;

import javax.ws.rs.core.Response;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class InputException extends ApiException {

    private String message;

    public InputException(String message) {
        this.message = message;
    }

    @Override
    public Integer code() {
        return 2;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public Response.Status httpCode() {
        return Response.Status.BAD_REQUEST;
    }
}
