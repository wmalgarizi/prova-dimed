package br.com.wagner.client.contract.v1.facade;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.mapper.ItemUpdatePrecoMapper;
import br.com.wagner.client.contract.v1.model.ItemUpdatePreco;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;
import br.com.wagner.client.impl.services.ItemUpdatePrecoService;

import java.util.List;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class ItemUpdatePrecoFacade {

    private ItemUpdatePrecoService itemUpdatePrecoService;

    public ItemUpdatePrecoFacade(ItemUpdatePrecoService itemUpdatePrecoService) {
        this.itemUpdatePrecoService = itemUpdatePrecoService;
    }

    public void alterarPrecoItem(List<ItemUpdatePreco> itensUpdatePreco) {
        if (itensUpdatePreco == null || itensUpdatePreco.isEmpty())
            throw new InputException("Itens devem ser selecionados para haver a alteração.");
        List<ItemUpdatePrecoModel> itens = ItemUpdatePrecoMapper.mapListParaImpl(itensUpdatePreco);
        itemUpdatePrecoService.alterarPreco(itens);
    }
}
