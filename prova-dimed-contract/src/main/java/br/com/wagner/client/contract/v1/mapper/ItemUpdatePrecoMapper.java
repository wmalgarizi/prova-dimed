package br.com.wagner.client.contract.v1.mapper;

import br.com.wagner.client.contract.v1.model.ItemUpdatePreco;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemUpdatePrecoMapper {

    public static List<ItemUpdatePrecoModel> mapListParaImpl(List<ItemUpdatePreco> itensUpdatePreco) {
        if (itensUpdatePreco == null) return null;
        return itensUpdatePreco.stream().map(ItemUpdatePrecoMapper::mapParaImpl).collect(Collectors.toList());
    }

    public static ItemUpdatePrecoModel mapParaImpl(ItemUpdatePreco itemUpdatePreco) {
        if (itemUpdatePreco == null) return null;
        return ItemUpdatePrecoModel.builder()
                .id(itemUpdatePreco.getId())
                .preco(itemUpdatePreco.getPreco())
                .build();
    }
}
