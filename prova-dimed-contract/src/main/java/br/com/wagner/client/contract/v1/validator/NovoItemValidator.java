package br.com.wagner.client.contract.v1.validator;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.NovoItem;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class NovoItemValidator {

    public static void validaNovoItem(NovoItem novoItem){
        if (novoItem == null)
            throw new InputException("Nenhum item recebido");
        if (novoItem.getNome() == null || novoItem.getNome().isEmpty())
            throw new InputException("O nome do item deve ser informado.");
        if (novoItem.getPreco() == null || novoItem.getPreco() == 0.0)
            throw new InputException("O preço do item deve ser informado.");
        NovaPromocaoValidator.validaNovaPromocao(novoItem.getPromocao());
    }
}
