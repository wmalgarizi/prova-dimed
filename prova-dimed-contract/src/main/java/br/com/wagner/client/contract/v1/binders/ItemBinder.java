package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.Item;
import br.com.wagner.client.impl.model.ItemModel;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class ItemBinder {

    public static Item bindParaContract(ItemModel itemModel) {
        return Item.builder()
                .id(itemModel.getId())
                .nome(itemModel.getNome())
                .descricao(itemModel.getDescricao())
                .preco(itemModel.getPreco())
                .promocao(PromocaoBinder.bindParaContract(itemModel.getPromocao()))
                .build();
    }

    public static ItemModel bindParaImpl(Item item) {
        return ItemModel.builder()
                .id(item.getId())
                .nome(item.getNome())
                .descricao(item.getDescricao())
                .preco(item.getPreco())
                .promocao(PromocaoBinder.bindParaImpl(item.getPromocao()))
                .build();
    }
}
