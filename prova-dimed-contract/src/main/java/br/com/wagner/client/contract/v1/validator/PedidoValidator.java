package br.com.wagner.client.contract.v1.validator;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.Pedido;
import io.swagger.models.auth.In;

/**
 * Created by wmalgarizi on 19/04/17.
 */
public class PedidoValidator {

    public static void validaPedido(Pedido pedido){
        if (pedido == null)
            throw new InputException("Nenhum pedido recebido.");
        if (pedido.getItens() == null || pedido.getItens().isEmpty())
            throw new InputException("Nenhum item foi adicionado ao pedido.");
        if (pedido.getIdCliente() == null || pedido.getIdCliente() == 0)
            throw new InputException("O código do cliente não foi informado.");
        if (pedido.getIdEndereco() == null || pedido.getIdEndereco() == 0)
            throw new InputException("O endereço para entrega não foi informado.");
        if (pedido.getEntrega() == null || pedido.getEntrega().isEmpty())
            throw new InputException("O tipo da entrega não foi informado.");
    }
}
