package br.com.wagner.client.contract.v1.facade;

import br.com.wagner.client.contract.v1.binders.ItemBinder;
import br.com.wagner.client.contract.v1.binders.NovoItemBinder;
import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.mapper.ItemMapper;
import br.com.wagner.client.contract.v1.mapper.ItemUpdatePrecoMapper;
import br.com.wagner.client.contract.v1.model.Item;
import br.com.wagner.client.contract.v1.model.ItemUpdatePreco;
import br.com.wagner.client.contract.v1.model.NovoItem;
import br.com.wagner.client.impl.model.ItemModel;
import br.com.wagner.client.impl.model.ItemUpdatePrecoModel;
import br.com.wagner.client.impl.services.ItemService;
import br.com.wagner.client.impl.services.ItemUpdatePrecoService;

import java.util.List;

/**
 * Created by wmalgarizi on 17/04/17.
 */

public class ItemFacade {

    private ItemService itemService;

    public ItemFacade(ItemService itemService) {
        this.itemService = itemService;
    }

    public void cadastrarNovoItem(NovoItem novoItem) {
        itemService.cadastrarItem(NovoItemBinder.bindParaItemModel(novoItem));
    }

    public Item procurarItemPorId(Long idItem){
        return ItemBinder.bindParaContract(itemService.procurarItemPorID(idItem));
    }

    public List<Item> procurarItensPorNome(String nomeItem) {
        if (nomeItem == null)
            throw new InputException("O nome de um item ou parte dele deve ser digitado para que a pesquisa seja realizada.");
        List<ItemModel> itens = itemService.procurarItensPorNome(nomeItem);
        return ItemMapper.mapListParaContract(itens);
    }

}
