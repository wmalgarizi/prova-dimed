package br.com.wagner.client.contract.v1.model;

import br.com.wagner.client.impl.model.EntregaModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by wmalgarizi on 19/04/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class Pedido {

    private List<Item> itens;
    private Long idCliente;
    private Long idEndereco;
    private String entrega;
    private String formaPagamento;
}
