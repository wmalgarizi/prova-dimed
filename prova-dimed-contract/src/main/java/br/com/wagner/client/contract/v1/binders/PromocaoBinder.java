package br.com.wagner.client.contract.v1.binders;

import br.com.wagner.client.contract.v1.model.Promocao;
import br.com.wagner.client.impl.model.PromocaoModel;

/**
 * Created by wmalgarizi on 17/04/17.
 */
public class PromocaoBinder {

    public static Promocao bindParaContract(PromocaoModel promocaoModel) {
        if(promocaoModel == null) return null;
        return Promocao.builder()
                .codigo(promocaoModel.getCodigo())
                .percentualDesconto(promocaoModel.getPercentualDesconto())
                .build();
    }

    public static PromocaoModel bindParaImpl(Promocao promocao) {
        if(promocao == null) return null;
        return PromocaoModel.builder()
                .codigo(promocao.getCodigo())
                .percentualDesconto(promocao.getPercentualDesconto())
                .build();
    }
}
