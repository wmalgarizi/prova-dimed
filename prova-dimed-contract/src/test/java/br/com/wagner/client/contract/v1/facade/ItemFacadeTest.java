package br.com.wagner.client.contract.v1.facade;

import br.com.wagner.client.contract.v1.exception.InputException;
import br.com.wagner.client.contract.v1.model.Item;
import br.com.wagner.client.impl.database.ItemData;
import br.com.wagner.client.impl.services.ItemService;
import br.com.wagner.client.impl.services.ItemUpdatePrecoService;
import br.com.wagner.client.impl.services.PromocaoService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by wmalgarizi on 18/04/17.
 */
public class ItemFacadeTest {

    private ItemFacade itemFacade;
    private ItemService itemService;

    @Before
    public void setUp(){
        this.itemService = mock(ItemService.class);
        this.itemFacade = new ItemFacade(itemService);
    }

}